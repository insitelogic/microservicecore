<?php

namespace InSiteLogic\Config;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ConfigServiceProvider implements ServiceProviderInterface {

	/**
	 * Registers services on the given container.
	 * This method should only be used to configure services and parameters.
	 * It should not get services.
	 * @param Container $pimple A container instance
	 */
	public function register(Container $pimple) {
		//load in configuration
		$pimple['config'] = function(Container $pimple) {
			return json_decode(file_get_contents($pimple['config.path']), true);
		};
	}
}