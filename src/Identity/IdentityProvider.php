<?php

namespace InSiteLogic\Identity;

use GuzzleHttp\Client;

class IdentityProvider {

	const IDENTITY_URL_PATH = "/identify/*";

	/**
	 * @var Client
	 */
	private $client;

	/**
	 * @var string
	 */
	private $baseUrl;

	/**
	 * @param string $baseUrl
	 * @return IdentityProvider
	 */
	public static function createProvider($baseUrl) {
		return new IdentityProvider($baseUrl);
	}

	/**
	 * IdentityProvider constructor.
	 *
	 * @param string $baseUrl
	 */
	public function __construct($baseUrl) {
		$this->baseUrl = $baseUrl;

		$this->client = new Client([
			'base_uri' => $this->baseUrl
		]);
	}

	/**
	 * @param int $id
	 * @return Identity
	 */
	public function request($id) {
		syslog(LOG_INFO, "insite IdentityProvider::request()");
		if (function_exists('curl_multi_exec') && function_exists('curl_exec')) {
			syslog(LOG_INFO, "curl_multi_exec and curl_exec are defined, use URLFetch");
			$payload = $this->makeUrlFetchRequest($id);
		} else if (function_exists('curl_exec')) {
			syslog(LOG_INFO, "curl_exec is defined, use URLFetch");
			$payload = $this->makeUrlFetchRequest($id);
		} else if (function_exists('curl_multi_exec')) {
			syslog(LOG_INFO, "curl_multi_exec is defined, use URLFetch");
			$payload = $this->makeUrlFetchRequest($id);
		} else {
			syslog(LOG_INFO, "no curl functions defined, use guzzle");
			$payload = $this->makeGuzzleRequest($id);
		}

		$data = $payload['data'];

		return new Identity(
			$data['id'],
			$data['companyName'],
			array_key_exists('baseFrame', $data) ? $data['baseFrame'] : null,
			array_key_exists('loginFrame', $data) ? $data['baseFrame'] : null,
			$data['companyHost'],
			$data['dbHost'],
			$data['dbUser'],
			$data['dbPass'],
			$data['dbName'],
			$data['dbPort'],
			$data['dbSock'],
            $data['companyLogo'],
			array_key_exists('dbReadReplica', $data) ? $data['dbReadReplica'] : null
		);
	}

	private function createRequestPath($id) {
		return str_replace('*', $id, IdentityProvider::IDENTITY_URL_PATH);
	}

	private function makeUrlFetchRequest($id) {
		$url = $this->createRequestPath($id);
		$context = [
			'http' => [
				'method' => 'GET',
			    'header' => "accept: */*\r\nContent-Type: application/json\r\n",
			    'follow_location' => 0
			]
		];

		$context = stream_context_create($context);
		$contents = file_get_contents($this->baseUrl . $url, false, $context);
		return json_decode($contents, true);
	}

	/**
	 * @param $id
	 * @return mixed
	 */
	private function makeGuzzleRequest($id) {
		//add allow_redirects => false to prevent being redirected
		$json    = $this->client->get($this->createRequestPath($id), ['allow_redirects' => false,  'verify' => false]);
		$payload = json_decode($json->getBody(), true);
		return $payload;
	}
}