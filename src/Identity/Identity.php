<?php

namespace InSiteLogic\Identity;

class Identity {
	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $companyName;

	/**
	 * @var string
	 */
	private $baseFrame;

	/**
	 * @var string
	 */
	private $loginFrame;

	/**
	 * @var string
	 */
	private $companyHost;

	/**
	 * @var string
	 */
	private $dbHost;

	/**
	 * @var string
	 */
	private $dbUser;

	/**
	 * @var string
	 */
	private $dbPass;

	/**
	 * @var string
	 */
	private $dbName;

	/**
	 * @var string
	 */
	private $dbPort;

	/**
	 * @var string
	 */
	private $dbSock;

	/**
	 * @var string
	 */
	private $companyLogo;

	/**
	 * @var string
	 */
	private $dbReadReplica;

	/**
	 * ProjectRecord constructor.
	 * @param int         $id
	 * @param string      $companyName
	 * @param string      $baseFrame
	 * @param string      $loginFrame
	 * @param string      $companyHost
	 * @param string      $dbHost
	 * @param string      $dbUser
	 * @param string      $dbPass
	 * @param string      $dbName
	 * @param string      $dbPort
	 * @param string      $dbSock
	 * @param string      $companyLogo
	 * @param null|string $dbReadReplica
	 */
	public function __construct($id, $companyName, $baseFrame, $loginFrame, $companyHost, $dbHost, $dbUser, $dbPass, $dbName, $dbPort, $dbSock, $companyLogo, $dbReadReplica = null) {
		$this->id            = $id;
		$this->companyName   = $companyName;
		$this->companyHost   = $companyHost;
		$this->baseFrame     = $baseFrame;
		$this->loginFrame    = $loginFrame;
		$this->dbHost        = $dbHost;
		$this->dbUser        = $dbUser;
		$this->dbPass        = $dbPass;
		$this->dbName        = $dbName;
		$this->dbPort        = $dbPort;
		$this->dbSock        = $dbSock;
		$this->companyLogo   = $companyLogo;
		$this->dbReadReplica = $dbReadReplica;
	}

	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id) {
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getCompanyName() {
		return $this->companyName;
	}

	/**
	 * @param string $companyName
	 */
	public function setCompanyName($companyName) {
		$this->companyName = $companyName;
	}

	/**
	 * @return string
	 */
	public function getBaseFrame() {
		return $this->baseFrame;
	}

	/**
	 * @param string $baseFrame
	 */
	public function setBaseFrame($baseFrame) {
		$this->baseFrame = $baseFrame;
	}

	/**
	 * @return string
	 */
	public function getLoginFrame() {
		return $this->loginFrame;
	}

	/**
	 * @param string $loginFrame
	 */
	public function setLoginFrame($loginFrame) {
		$this->loginFrame = $loginFrame;
	}

	/**
	 * @return string
	 */
	public function getCompanyHost() {
		return $this->companyHost;
	}

	/**
	 * @param string $companyHost
	 */
	public function setCompanyHost($companyHost) {
		$this->companyHost = $companyHost;
	}

	/**
	 * @return string
	 */
	public function getDbHost() {
		return $this->dbHost;
	}

	/**
	 * @param string $dbHost
	 */
	public function setDbHost($dbHost) {
		$this->dbHost = $dbHost;
	}

	/**
	 * @return string
	 */
	public function getDbUser() {
		return $this->dbUser;
	}

	/**
	 * @param string $dbUser
	 */
	public function setDbUser($dbUser) {
		$this->dbUser = $dbUser;
	}

	/**
	 * @return string
	 */
	public function getDbPass() {
		return $this->dbPass;
	}

	/**
	 * @param string $dbPass
	 */
	public function setDbPass($dbPass) {
		$this->dbPass = $dbPass;
	}

	/**
	 * @return string
	 */
	public function getDbName() {
		return $this->dbName;
	}

	/**
	 * @param string $dbName
	 */
	public function setDbName($dbName) {
		$this->dbName = $dbName;
	}

	/**
	 * @return string
	 */
	public function getDbPort() {
		return $this->dbPort;
	}

	/**
	 * @param string $dbPort
	 */
	public function setDbPort($dbPort) {
		$this->dbPort = $dbPort;
	}

	/**
	 * @return string
	 */
	public function getDbSock() {
		return $this->dbSock;
	}

	/**
	 * @param string $dbSock
	 */
	public function setDbSock($dbSock) {
		$this->dbSock = $dbSock;
	}

	/**
	 * @return string
	 */
	public function getCompanyLogo() {
		return $this->companyLogo;
	}

	/**
	 * @param string $companyLogo
	 */
	public function setCompanyLogo($companyLogo) {
		$this->companyLogo = $companyLogo;
	}

	/**
	 * @return string
	 */
	public function getDbReadReplica() {
		return $this->dbReadReplica;
	}

	/**
	 * @param string $dbReadReplica
	 */
	public function setDbReadReplica($dbReadReplica) {
		$this->dbReadReplica = $dbReadReplica;
	}

	/**
	 * @return bool
	 */
	public function hasReadReplica() {
		return $this->dbReadReplica !== null;
	}

	/**
	 * @return array
	 */
	public function getReadReplicaConfiguration() {
		return json_decode($this->dbReadReplica, true);
	}
}