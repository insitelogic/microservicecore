<?php

namespace InSiteLogic\Identity\Provider;

use InSiteLogic\Identity\IdentityProvider;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class IdentityServiceProvider implements ServiceProviderInterface {

	const IS_MANGLED_IDENTITY_KEY = 'HTTP_IS_IDENTITY';

	/**
	 * Registers services on the given container.
	 * This method should only be used to configure services and parameters.
	 * It should not get services.
	 * @param Container $pimple A container instance
	 */
	public function register(Container $pimple) {
		$pimple['identity'] = function (Container $pimple) {
			syslog(LOG_INFO, "loading database identity");
			return IdentityProvider::createProvider($pimple['identity.service'])->request($_SERVER[IdentityServiceProvider::IS_MANGLED_IDENTITY_KEY]);
		};
	}
}