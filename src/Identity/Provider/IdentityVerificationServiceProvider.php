<?php

namespace InSiteLogic\Identity\Provider;

use Exception;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Api\BootableProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class IdentityVerificationServiceProvider implements ServiceProviderInterface, BootableProviderInterface {

	const IS_IDENTITY_KEY = 'IS-Identity';

	/**
	 * Bootstraps the application.
	 * This method is called after all services are registered
	 * and should be used for "dynamic" configuration (whenever
	 * a service must be requested).
	 * @param Application $app
	 */
	public function boot(Application $app) {
		//check for existence of IS-Identity header
		$app->before(function (Request $request) {
			if(strtolower($request->getMethod()) !== "options") {
				syslog(LOG_INFO, "checking IS-Identity exists");
				if(!$request->headers->has(IdentityVerificationServiceProvider::IS_IDENTITY_KEY)) {
					throw new Exception('missing IS-Identity header in request');
				} else {
					$isIdentity = $request->headers->get(IdentityVerificationServiceProvider::IS_IDENTITY_KEY);
					syslog(LOG_INFO, "request has identity id of: $isIdentity");
				}
			}
		});
	}

	/**
	 * Registers services on the given container.
	 * This method should only be used to configure services and parameters.
	 * It should not get services.
	 * @param Container $pimple A container instance
	 */
	public function register(Container $pimple) {}
}