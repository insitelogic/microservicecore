<?php

namespace InSiteLogic\Util;

use ReflectionClass;
use Zend\Hydrator\HydratorInterface;
use Zend\Hydrator\Reflection;

class ObjectHydrator {

	/**
	 * @var Reflection
	 */
	private static $defaultHydrator;

	/**
	 * @var HydratorInterface
	 */
	private $hydratorInstance;

	/**
	 * @return ObjectHydrator
	 */
	public static function getDefaultHydrator() {
		if(self::$defaultHydrator === null) {
			self::$defaultHydrator = new ObjectHydrator(new Reflection());
		}

		return self::$defaultHydrator;
	}

	/**
	 * @param HydratorInterface $hydrator
	 * @return ObjectHydrator
	 */
	public static function getHydrator(HydratorInterface $hydrator) {
		return new ObjectHydrator($hydrator);
	}

	/**
	 * ObjectHydrator constructor.
	 * @param HydratorInterface $hydratorInstance
	 */
	public function __construct(HydratorInterface $hydratorInstance) {
		$this->hydratorInstance = $hydratorInstance;
	}

	/**
	 * @param array $data
	 * @param string $class
	 * @return object|null
	 */
	public function hydrate(array $data, $class) {
		$result = null;

		if($data !== null) {
			$instance = $this->createInstance($class);
			$result = $this->hydratorInstance->hydrate($data, $instance);
		}

		return $result;
	}

	/**
	 * @param $class
	 * @return object
	 */
	private function createInstance($class) {
		$reflectionClass = new ReflectionClass($class);

		//return new instance without constructor to ensure no populated params
		return $reflectionClass->newInstanceWithoutConstructor();
	}
}