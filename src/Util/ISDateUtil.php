<?php

namespace InSiteLogic\Util;

use DateInterval;
use DateTime;
use DateTimeImmutable;
use DateTimeZone;

class ISDateUtil {

	/**
	 * standard format for date-time strings going into and coming out of mysql
	 */
	const MYSQL_DATETIME_FORMAT = "Y-m-d H:i:s";

	/**
	 * standard format for date-only strings going into and coming out of mysql
	 */
	const MYSQL_DATE_FORMAT = "Y-m-d";

	/**
	 * The textual constant for the melbourne timezone
	 */
	const AUSTRALIA_MELB_TIMEZONE = "Australia/Melbourne";

	/**
	 * The textual constant for the UTC timezone
	 */
	const UTC_TIMEZONE = "UTC";

	/**
	 * @var DateTime
	 */
	protected $dateTimeObj;

	/**
	 * @var string
	 */
	protected $dateTimeStr;

	/**
	 * Due to createFromMutable only existing in PHP5.6+, this is a utility method that
	 * provides the same functionality
	 *
	 * @param DateTime $dateTime
	 * @return DateTimeImmutable
	 */
	public static function createImmutableDateTime(DateTime $dateTime) {
		return DateTimeImmutable::createFromFormat('U', $dateTime->getTimestamp(), $dateTime->getTimezone());
	}

	/**
	 * Builds an ISDateUtil object using a provided string in MySQL format
	 *
	 * @param string $dateTimeString
	 * @param bool $isDateOnly
	 * @return ISDateUtil
	 */
	public static function build($dateTimeString, $isDateOnly = false) {
		return new ISDateUtil($dateTimeString, $isDateOnly);
	}

	/**
	 * Builds an ISDateUtil object using a provided unix timestamp
	 *
	 * @param int $timestamp
	 * @return ISDateUtil
	 */
	public static function buildFromTimestamp($timestamp) {
		$tmp = new DateTime();
		$tmp->setTimestamp($timestamp);
		return new ISDateUtil($tmp->format(self::MYSQL_DATETIME_FORMAT), false);
	}

	/**
	 * Builds an ISDateUtil object using a provided DateTime object
	 *
	 * @param DateTime $dateTime
	 * @return ISDateUtil
	 */
	public static function buildFromDateObject(DateTime $dateTime) {
		return new ISDateUtil($dateTime->format(self::MYSQL_DATETIME_FORMAT), false);
	}

	/**
	 * Given a DateTime object in UTC, return a new object containing the same date as the
	 * parameter converted to Australia/Melbourne timezone.
	 *
	 * @param DateTime $dateTime
	 * @return bool|DateTimeImmutable
	 */
	public static function convertUTCToMelbourne(DateTime $dateTime) {
		$immutable = self::createImmutableDateTime($dateTime);
		$newDateTime = $immutable->setTimezone(new DateTimeZone(self::AUSTRALIA_MELB_TIMEZONE));
		return $newDateTime;
	}

	/**
	 * Given a DateTime object in Australia/Melbourne, return a new object containing the same
	 * date as the parameter converted to UTC timezone.
	 *
	 * @param DateTime $dateTime
	 * @return bool|DateTimeImmutable
	 */
	public static function convertMelbourneToUTC(DateTime $dateTime) {
		$immutable = self::createImmutableDateTime($dateTime);
		$newDateTime = $immutable->setTimezone(new DateTimeZone(self::UTC_TIMEZONE));
		return $newDateTime;
	}

	/**
	 * ISDateUtil constructor.
	 *
	 * @param string $dateTimeStr
	 * @param bool $isDateOnly
	 */
	public function __construct($dateTimeStr, $isDateOnly) {
		$this->dateTimeStr = $dateTimeStr;

		if($isDateOnly) {
			$this->dateTimeObj = DateTime::createFromFormat(self::MYSQL_DATE_FORMAT, $dateTimeStr);
		} else {
			$this->dateTimeObj = DateTime::createFromFormat(self::MYSQL_DATETIME_FORMAT, $dateTimeStr);
		}
	}

	/**
	 * returns the Unix Timestamp represented by the internal DateTime object
	 *
	 * @return int
	 */
	public function getTimeStamp() {
		return $this->dateTimeObj->getTimestamp();
	}

	/**
	 * returns the internally stored DateTime object as string formatted for inserting into MySQL
	 *
	 * @param bool $dateOnly
	 * @return string
	 */
	public function asMysqlString($dateOnly = false) {
		if($dateOnly) {
			return $this->dateTimeObj->format(self::MYSQL_DATE_FORMAT);
		} else {
			return $this->dateTimeObj->format(self::MYSQL_DATETIME_FORMAT);
		}
	}

	/**
	 * returns the internally stored DateTime object
	 *
	 * @return DateTime
	 */
	public function getDateTimeObject() {
		return $this->dateTimeObj;
	}

	/**
	 * returns an DateTimeImmutable object which represents the stored object incremented by $numberDays. This is a new object and
	 * does not affect DateTime object stored in this class.
	 *
	 * @param int $numberDays
	 * @return DateTimeImmutable
	 */
	public function incrementByDays($numberDays) {
		$interval = DateInterval::createFromDateString("+$numberDays day");
		$immutable = self::createImmutableDateTime($this->dateTimeObj);
		return $immutable->add($interval);
	}

	/**
	 * returns an DateTimeImmutable object which represents the stored object decremented by $numberDays. This is a new object and
	 * does not affect DateTime object stored in this class.
	 *
	 * @param int $numberDays
	 * @return DateTimeImmutable
	 */
	public function decrementByDays($numberDays) {
		$interval = DateInterval::createFromDateString("-$numberDays day");
		$immutable = self::createImmutableDateTime($this->dateTimeObj);
		return $immutable->add($interval);
	}

	/**
	 * returns an DateTimeImmutable object which represents the stored object modified relative to $relativeTimeString. This is a
	 * new object and does not affect DateTime object stored in this class.
	 *
	 * @param string $relativeTimeString
	 * @return DateTimeImmutable
	 */
	public function modifyByRelativeTimeString($relativeTimeString) {
		$interval = DateInterval::createFromDateString($relativeTimeString);
		$immutable = self::createImmutableDateTime($this->dateTimeObj);
		return $immutable->add($interval);
	}
}