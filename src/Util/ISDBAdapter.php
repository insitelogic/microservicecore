<?php

namespace InSiteLogic\Util;

use mysqli;

class ISDBAdapter {

	/**
	 * @var mysqli
	 */
	private $mysqliRead;

	/**
	 * @var mysqli|null
	 */
	private $mysqliWrite;

	/**
	 * @var array
	 */
	private $queryCache;

	/**
	 * @var
	 */
	private $affectedRows = null;

	/**
	 * ISDBAdapter constructor.
	 * @param mysqli      $mysqliRead
	 * @param mysqli|null $mysqliWrite
	 */
	public function __construct(mysqli $mysqliRead, mysqli $mysqliWrite = null) {
		$this->mysqliRead  = $mysqliRead;
		$this->mysqliWrite = $mysqliWrite;
		$this->queryCache  = [];
	}

	/**
	 * execute a query that returns an array of multiple results, if the query has been
	 * run before while the current object is still alive, then return a cached copy
	 * of the query result for performance reasons
	 * if you are not binding any parameters into the query, you can provide an empty
	 * array to $bindParams and a null-value to $bindTypeStr
	 * @param string      $query
	 * @param array       $bindParams
	 * @param null|string $bindTypeStr
	 * @param bool        $useCache
	 * @return array
	 */
	public function queryDBForMultiple($query, array $bindParams, $bindTypeStr = null, $useCache = true) {
		//if the current query is cached, return the cached value, otherwise, query the
		//DB for the result and cache it
		if ($this->isResultCached($query) && $useCache) {
			return $this->fetchResultFromCache($query);
		} else {
			$result = $this->executeQuery($query, $bindParams, $bindTypeStr);
			$this->storeResultsInCache($query, $result);

			return $result;
		}
	}

	/**
	 * execute a query that returns a single result, if the query has been
	 * run before while the current object is still alive, then return a cached copy
	 * of the query result for performance reasons
	 * if you are not binding any parameters into the query, you can provide an empty
	 * array to $bindParams and a null-value to $bindTypeStr
	 * @param string      $query
	 * @param array       $bindParams
	 * @param null|string $bindTypeStr
	 * @param bool        $useCache
	 * @return array
	 */
	public function queryDBForSingle($query, array $bindParams, $bindTypeStr = null, $useCache = true) {
		$returnVal = null;

		//if the current query is cached, return the cached value, otherwise, query the
		//DB for the result and cache it
		if ($this->isResultCached($query) && $useCache) {
			$returnVal = $this->fetchResultFromCache($query);
		} else {
			$qryResult = $this->executeQuery($query, $bindParams, $bindTypeStr);

			if ($qryResult !== null && count($qryResult) > 0) {
				$returnVal = $qryResult[0];
			}

			$this->storeResultsInCache($query, $returnVal);
		}

		return $returnVal;
	}

	/**
	 * execute an update/write query that does not return anything
	 * @param string      $query
	 * @param array       $bindParams
	 * @param null|string $bindTypeStr
	 * @return boolean
	 */
	public function runUpdateQuery($query, array $bindParams, $bindTypeStr = null) {
		$result = false;
		$stmt   = $this->fetchConnection(false)->prepare($query);
		if ($stmt) {
			//if bindParams has values, we bind those values to the query
			if (count($bindParams) > 0 || $bindTypeStr !== null) {
				/* with call_user_func_array, array params must be passed by reference */
				$params   = [];
				$params[] = &$bindTypeStr;

				for ($i = 0; $i < count($bindParams); $i++) {
					/* with call_user_func_array, array params must be passed by reference */
					$params[] = &$bindParams[$i];
				}

				call_user_func_array([$stmt, 'bind_param'], $params);
			}

			$result = $stmt->execute();
			if ($result === false) {
				syslog(LOG_ERR, "Failed to run query: $query");
				syslog(LOG_ERR, $stmt->error);
				syslog(LOG_ERR, print_r($stmt->error_list, true));
			} else {
				$this->affectedRows = $stmt->affected_rows;
			}
		} else {
			syslog(LOG_ERR, "Failed to prepare stmt");
			syslog(LOG_ERR, $this->fetchConnection(false)->error);
		}

		return $result;
	}

	/**
	 * helper function that dynamically binds the given $bindParams to the query
	 * in an sql-injection safe manner.
	 * $bindParams and $bindTypeStr are optional, and if you provide an empty array
	 * and null-value respectively, then no values will be bound to the query
	 * @param             $query
	 * @param array       $bindParams
	 * @param null|string $bindTypeStr
	 * @return array
	 */
	private function executeQuery($query, array $bindParams = [], $bindTypeStr = null) {
		$result = null;
		$stmt   = $this->fetchConnection()->prepare($query);
		if ($stmt) {
			//if bindParams has values, we bind those values to the query
			if (count($bindParams) > 0 || $bindTypeStr !== null) {
				/* with call_user_func_array, array params must be passed by reference */
				$params   = [];
				$params[] = &$bindTypeStr;

				for ($i = 0; $i < count($bindParams); $i++) {
					/* with call_user_func_array, array params must be passed by reference */
					$params[] = &$bindParams[$i];
				}

				call_user_func_array([$stmt, 'bind_param'], $params);
			}

			$res = $stmt->execute();
			if ($res) {
				$this->affectedRows = $stmt->affected_rows;
				$qryResult          = $stmt->get_result();
				$count              = $qryResult->num_rows;
				syslog(LOG_INFO, "number of results: $count");

				//init result as an array and pull down results
				$result = [];
				while ($row = $qryResult->fetch_assoc()) {
					$result[] = $row;
				}
			} else {
				syslog(LOG_ERR, 'execute() failed: ' . htmlspecialchars($stmt->error));
			}

			//cleanup
			$stmt->close();
		} else {
			syslog(LOG_ERR, "Failed to prepare stmt");
			syslog(LOG_ERR, $this->fetchConnection()->error);
		}

		return $result;
	}

	/**
	 * @return int
	 */
	public function getLastUpdatedId() {
		return $this->mysqliRead->insert_id;
	}

	/**
	 * @return mixed
	 */
	public function getAffectedRows() {
		return $this->affectedRows;
	}

	/**
	 * execute multi_query on supplied query
	 * @param string $query
	 * @return array
	 */
	public function executeMultiQuery($query) {
		$res  = null;
		$stmt = $this->fetchConnection();
		if ($stmt) {
			$res = $stmt->multi_query($query);
			if ($res) {
				syslog(LOG_INFO, 'Multi Query success');
			} else {
				syslog(LOG_ERR, 'Multi Query failed: ' . htmlspecialchars($stmt->error));
			}
			$stmt->close();
		} else {
			syslog(LOG_ERR, 'Failed to fetchConnection');
			syslog(LOG_ERR, $this->fetchConnection()->error);
		}

		return $res;
	}

	/**
	 * returns true if the result of the given query is cached during the current execution context
	 * @param string $key
	 * @return bool
	 */
	private function isResultCached($key) {
		if (array_key_exists($key, $this->queryCache)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * return the cached value of the given query if it exists, otherwise return null.
	 * @param $key
	 * @return mixed|null
	 */
	private function fetchResultFromCache($key) {
		if ($this->isResultCached($key)) {
			return $this->queryCache[$key];
		} else {
			return null;
		}
	}

	/**
	 * store the results of the given query, the stored results will only be cached during the
	 * lifetime of the current object, meaning that when the executing script finishes, the cache
	 * will no longer exist
	 * @param string $key
	 * @param mixed  $val
	 */
	private function storeResultsInCache($key, $val) {
		$this->queryCache[$key] = $val;
	}

	private function fetchConnection($isRead=true) {
		if($this->mysqliWrite !== null && !$isRead) {
			return $this->mysqliWrite;
		} else {
			return $this->mysqliRead;
		}
	}



}