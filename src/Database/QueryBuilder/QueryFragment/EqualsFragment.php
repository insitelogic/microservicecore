<?php
/**
 * Created by PhpStorm.
 * User: insite-three
 * Date: 28/10/2016
 * Time: 12:20 PM
 */

namespace InSiteLogic\Database\QueryBuilder\QueryFragment;

class EqualsFragment implements QueryFragment {

	/**
	 * @var string
	 */
	protected $key;

	/**
	 * @var string
	 */
	protected $val;

	/**
	 * WhereFragment constructor.
	 * @param string $whereKey
	 * @param string $whereVal
	 */
	public function __construct($whereKey, $whereVal) {
		$this->key = $whereKey;
		$this->val = $whereVal;
	}

	/**
	 * @return string
	 */
	public function generate() {
		return $this->key . "=" . $this->val;
	}
}