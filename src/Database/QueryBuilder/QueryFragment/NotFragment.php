<?php
/**
 * Created by PhpStorm.
 * User: insite-three
 * Date: 28/10/2016
 * Time: 12:19 PM
 */

namespace InSiteLogic\Database\QueryBuilder\QueryFragment;

class NotFragment implements QueryFragment {

	/**
	 * @return string
	 */
	public function generate() {
		return "NOT";
	}
}