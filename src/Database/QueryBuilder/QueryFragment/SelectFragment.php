<?php

namespace InSiteLogic\Database\QueryBuilder\QueryFragment;

class SelectFragment implements QueryFragment {

	/**
	 * @var array
	 */
	private $columnNames;

	/**
	 * SelectFragment constructor.
	 * @param array $columnNames
	 */
	public function __construct(array $columnNames) { $this->columnNames = $columnNames; }

	/**
	 * @return string
	 */
	public function generate() {
		return "SELECT " . $this->generateColumnNamesString();
	}

	/**
	 * @return string
	 */
	private function generateColumnNamesString() {
		return implode(", ", $this->columnNames);
	}
}