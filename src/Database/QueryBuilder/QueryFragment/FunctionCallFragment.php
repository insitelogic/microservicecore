<?php

namespace InSiteLogic\Database\QueryBuilder\QueryFragment;

class FunctionCallFragment implements QueryFragment {

	/**
	 * @var string
	 */
	private $functionName;

	/**
	 * @var array
	 */
	private $params;

	/**
	 * FunctionCallFragment constructor.
	 * @param string $functionName
	 * @param array  $params
	 */
	public function __construct($functionName, array $params) {
		$this->functionName = $functionName;
		$this->params       = $params;
	}

	/**
	 * @return string
	 */
	public function generate() {
		$funcName = strtoupper($this->functionName);
		$params = $this->generateArgsString();
		return "$funcName($params)";
	}

	public function __toString() {
		return $this->generate();
	}

	private function generateArgsString() {
		return implode(', ', $this->params);
	}
}