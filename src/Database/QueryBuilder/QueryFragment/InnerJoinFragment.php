<?php

namespace InSiteLogic\Database\QueryBuilder\QueryFragment;

class InnerJoinFragment extends JoinFragment {

	/**
	 * @return string
	 */
	protected function joinType() {
		return "INNER ";
	}
}