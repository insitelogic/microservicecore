<?php

namespace InSiteLogic\Database\QueryBuilder\QueryFragment;

class WhereFragment implements QueryFragment {

	/**
	 * @return string
	 */
	public function generate() {
		return "WHERE";
	}
}