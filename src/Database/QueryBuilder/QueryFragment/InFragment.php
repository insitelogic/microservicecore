<?php

namespace InSiteLogic\Database\QueryBuilder\QueryFragment;

class InFragment implements QueryFragment {

	/**
	 * @var string
	 */
	private $lval;

	/**
	 * @var array
	 */
	private $valueList;

	/**
	 * InFragment constructor.
	 *
	 * @param string $lval
	 * @param array  $valueList
	 */
	public function __construct($lval, array $valueList) {
		$this->lval      = $lval;
		$this->valueList = $valueList;
	}

	/**
	 * @return string
	 */
	public function generate() {
		$inList = $this->generateInString();
		return $this->lval . " IN ($inList)";
	}

	private function generateInString() {
		return implode(', ', $this->valueList);
	}
}