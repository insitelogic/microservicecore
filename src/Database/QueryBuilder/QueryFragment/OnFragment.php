<?php

namespace InSiteLogic\Database\QueryBuilder\QueryFragment;

class OnFragment implements QueryFragment {

	/**
	 * @var string
	 */
	private $lval;

	/**
	 * @var string
	 */
	private $rval;

	/**
	 * OnFragment constructor.
	 *
	 * @param string $lval
	 * @param string $rval
	 */
	public function __construct($lval, $rval) {
		$this->lval = $lval;
		$this->rval = $rval;
	}

	/**
	 * @return string
	 */
	public function generate() {
		$lval = $this->lval;
		$rval = $this->rval;
		return "ON $lval=$rval";
	}
}