<?php

namespace InSiteLogic\Database\QueryBuilder\QueryFragment;

abstract class ComparisonFragment implements QueryFragment {

	/**
	 * @var string
	 */
	private $lval;

	/**
	 * @var string
	 */
	private $rval;

	/**
	 * ComparisonFragment constructor.
	 * @param string $lval
	 * @param string $rval
	 */
	public function __construct($lval, $rval) {
		$this->lval = $lval;
		$this->rval = $rval;
	}

	/**
	 * @return string
	 */
	public function generate() {
		return $this->lval . $this->getComparisonOperator() . $this->rval;
	}

	/**
	 * @return string
	 */
	abstract protected function getComparisonOperator();
}