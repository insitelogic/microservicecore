<?php

namespace InSiteLogic\Database\QueryBuilder\QueryFragment;

class FullJoinFragment extends JoinFragment {

	/**
	 * @return string
	 */
	protected function joinType() {
		return "FULL ";
	}
}