<?php

namespace InSiteLogic\Database\QueryBuilder\QueryFragment;

class FromFragment implements QueryFragment {

	/**
	 * @var array
	 */
	private $tableNames;

	/**
	 * FromFragment constructor.
	 * @param array $tableNames
	 */
	public function __construct(array $tableNames) { $this->tableNames = $tableNames; }

	/**
	 * @return string
	 */
	public function generate() {
		return "FROM " . $this->generateTableNamesString();
	}

	/**
	 * @return string
	 */
	private function generateTableNamesString() {
		return implode(', ', $this->tableNames);
	}
}