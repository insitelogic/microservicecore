<?php

namespace InSiteLogic\Database\QueryBuilder\QueryFragment;

class LessThanFragment extends ComparisonFragment {

	/**
	 * @return string
	 */
	protected function getComparisonOperator() {
		return "<";
	}
}