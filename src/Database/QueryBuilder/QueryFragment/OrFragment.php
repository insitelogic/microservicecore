<?php

namespace InSiteLogic\Database\QueryBuilder\QueryFragment;

class OrFragment implements QueryFragment {

	/**
	 * @return string
	 */
	public function generate() {
		return "OR";
	}
}