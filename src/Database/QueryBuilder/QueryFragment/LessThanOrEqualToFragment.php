<?php

namespace InSiteLogic\Database\QueryBuilder\QueryFragment;

class LessThanOrEqualToFragment extends ComparisonFragment {

	/**
	 * @return string
	 */
	protected function getComparisonOperator() {
		return "<=";
	}
}