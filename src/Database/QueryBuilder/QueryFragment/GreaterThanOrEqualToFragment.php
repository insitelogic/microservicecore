<?php

namespace InSiteLogic\Database\QueryBuilder\QueryFragment;

class GreaterThanOrEqualToFragment extends ComparisonFragment {

	/**
	 * @return string
	 */
	protected function getComparisonOperator() {
		return ">=";
	}
}