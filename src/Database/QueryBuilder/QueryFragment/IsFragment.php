<?php

namespace InSiteLogic\Database\QueryBuilder\QueryFragment;

class IsFragment implements QueryFragment {

	/**
	 * @return string
	 */
	public function generate() {
		return "IS";
	}
}