<?php

namespace InSiteLogic\Database\QueryBuilder\QueryFragment;

class LeftJoinFragment extends JoinFragment {

	/**
	 * @return string
	 */
	protected function joinType() {
		return "LEFT ";
	}
}