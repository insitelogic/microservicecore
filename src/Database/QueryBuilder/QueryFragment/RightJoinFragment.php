<?php

namespace InSiteLogic\Database\QueryBuilder\QueryFragment;

class RightJoinFragment extends JoinFragment {

	/**
	 * @return string
	 */
	protected function joinType() {
		return "RIGHT ";
	}
}