<?php

namespace InSiteLogic\Database\QueryBuilder\QueryFragment;

class LikeFragment implements QueryFragment {

	public function generate() {
		return "LIKE";
	}
}