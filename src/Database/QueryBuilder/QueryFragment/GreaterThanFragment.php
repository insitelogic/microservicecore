<?php

namespace InSiteLogic\Database\QueryBuilder\QueryFragment;

class GreaterThanFragment extends ComparisonFragment {

	/**
	 * @return string
	 */
	protected function getComparisonOperator() {
		return ">";
	}
}