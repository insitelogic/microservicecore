<?php

namespace InSiteLogic\Database\QueryBuilder\QueryFragment;

interface QueryFragment {

	/**
	 * @return string
	 */
	public function generate();
}