<?php

namespace InSiteLogic\Database\QueryBuilder\QueryFragment;

class NotEqualsFragment extends ComparisonFragment {

	/**
	 * @return string
	 */
	protected function getComparisonOperator() {
		return "!=";
	}
}