<?php

namespace InSiteLogic\Database\QueryBuilder\QueryFragment;

abstract class JoinFragment implements QueryFragment {

	/**
	 * @var string
	 */
	protected $table;

	/**
	 * JoinFragment constructor.
	 * @param string $table
	 */
	public function __construct($table) {
		$this->table = $table;
	}

	public function generate() {
		return $this->joinType() . "JOIN " . $this->table;
	}

	abstract protected function joinType();
}