<?php

namespace InSiteLogic\Database\QueryBuilder\QueryFragment;

class AndFragment implements QueryFragment {

	/**
	 * @return string
	 */
	public function generate() {
		return "AND";
	}
}