<?php

namespace InSiteLogic\Database\QueryBuilder\QueryFragment;

class RawFragment implements QueryFragment {

	/**
	 * @var string
	 */
	private $rawFragment;

	/**
	 * RawFragment constructor.
	 *
	 * @param string $rawFragment
	 */
	public function __construct($rawFragment) { $this->rawFragment = $rawFragment; }

	/**
	 * @return string
	 */
	public function generate() {
		return $this->rawFragment;
	}
}