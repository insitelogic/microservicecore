<?php

namespace InSiteLogic\Database\QueryBuilder;

use InSiteLogic\Database\BoundParameter;
use InSiteLogic\Database\Query\Impl\Query;
use InSiteLogic\Database\Query\QueryInterface;
use InSiteLogic\Database\QueryBuilder\QueryFragment\EqualsFragment;
use InSiteLogic\Database\QueryBuilder\QueryFragment\FromFragment;
use InSiteLogic\Database\QueryBuilder\QueryFragment\FullJoinFragment;
use InSiteLogic\Database\QueryBuilder\QueryFragment\FunctionCallFragment;
use InSiteLogic\Database\QueryBuilder\QueryFragment\GreaterThanFragment;
use InSiteLogic\Database\QueryBuilder\QueryFragment\GreaterThanOrEqualToFragment;
use InSiteLogic\Database\QueryBuilder\QueryFragment\InFragment;
use InSiteLogic\Database\QueryBuilder\QueryFragment\InnerJoinFragment;
use InSiteLogic\Database\QueryBuilder\QueryFragment\LeftJoinFragment;
use InSiteLogic\Database\QueryBuilder\QueryFragment\LessThanFragment;
use InSiteLogic\Database\QueryBuilder\QueryFragment\LessThanOrEqualToFragment;
use InSiteLogic\Database\QueryBuilder\QueryFragment\LikeFragment;
use InSiteLogic\Database\QueryBuilder\QueryFragment\NotEqualsFragment;
use InSiteLogic\Database\QueryBuilder\QueryFragment\OnFragment;
use InSiteLogic\Database\QueryBuilder\QueryFragment\QueryFragment;
use InSiteLogic\Database\QueryBuilder\QueryFragment\RawFragment;
use InSiteLogic\Database\QueryBuilder\QueryFragment\RightJoinFragment;
use InSiteLogic\Database\QueryBuilder\QueryFragment\SelectFragment;
use InSiteLogic\Database\QueryBuilder\QueryFragment\WhereFragment;
use InSiteLogic\Util\ISDBAdapter;

/**
 * Class QueryBuilder
 *
 * Builder interface for programmatically constructing queries.
 *
 * @package InSiteLogic\Database\QueryBuilder
 */
class QueryBuilder {

	/**
	 * @var QueryFragment[]
	 */
	private $queryFragments = [];

	/**
	 * @var BoundParameter[]
	 */
	private $boundParams = [];

	/**
	 * @var ISDBAdapter
	 */
	private $adapter;

	/**
	 * @param ISDBAdapter $adapter
	 * @return QueryBuilder
	 */
	public static function begin(ISDBAdapter $adapter) {
		return new QueryBuilder($adapter);
	}

	/**
	 * QueryBuilder constructor.
	 * @param ISDBAdapter $adapter
	 */
	public function __construct(ISDBAdapter $adapter) {
		$this->adapter = $adapter;
	}

	/**
	 * appends a select statement to the programatically constructed query
	 * @param array $columnNames
	 * @return QueryBuilder
	 */
	public function select(array $columnNames) {
		$this->queryFragments[] = new SelectFragment($columnNames);
		return $this;
	}

	public function selectCallback($callback) {
		$columnNames = $callback($this);
		return $this->select($columnNames);
	}

	/**
	 * appends a from statement to the programatically constructed query
	 * @param array $tableNames
	 * @return QueryBuilder
	 */
	public function from(array $tableNames) {
		$this->queryFragments[] = new FromFragment($tableNames);
		return $this;
	}

	/**
	 * appends a where clause to the programatically constructed query
	 * @return QueryBuilder
	 */
	public function where() {
		$this->queryFragments[] = new WhereFragment();
		return $this;
	}

	/**
	 * appends an equality comparison operation to the constructed query
	 * @param string $whereKey
	 * @param mixed  $whereVal
	 * @return QueryBuilder
	 */
	public function equals($whereKey, $whereVal) {
		$this->queryFragments[] = new EqualsFragment($whereKey, $whereVal);
		return $this;
	}

	/**
	 * appends a not-equals comparison operation to the constructed query
	 * @param string $whereKey
	 * @param mixed  $whereVal
	 * @return QueryBuilder
	 */
	public function notEquals($whereKey, $whereVal) {
		$this->queryFragments[] = new NotEqualsFragment($whereKey, $whereVal);
		return $this;
	}

	/**
	 * appends a greater-than comparison operation to the constructed query
	 * @param string $lval
	 * @param string $rval
	 * @return QueryBuilder
	 */
	public function greaterThan($lval, $rval) {
		$this->queryFragments[] = new GreaterThanFragment($lval, $rval);
		return $this;
	}

	/**
	 * appends a less-than comparison operation to the constructed query
	 * @param string $lval
	 * @param string $rval
	 * @return QueryBuilder
	 */
	public function lessThan($lval, $rval) {
		$this->queryFragments[] = new LessThanFragment($lval, $rval);
		return $this;
	}

	/**
	 * appends a greater-than-or-equal-to comparison operation to the constructed query
	 * @param string $lval
	 * @param string $rval
	 * @return QueryBuilder
	 */
	public function greaterThanOrEqualTo($lval, $rval) {
		$this->queryFragments[] = new GreaterThanOrEqualToFragment($lval, $rval);
		return $this;
	}

	/**
	 * appends a less-than-or-equal-to comparison operation to the constructed query
	 * @param string $lval
	 * @param string $rval
	 * @return QueryBuilder
	 */
	public function lessThanOrEqualTo($lval, $rval) {
		$this->queryFragments[] = new LessThanOrEqualToFragment($lval, $rval);
		return $this;
	}

	/**
	 * appends the table declaration of an inner-join
	 * @param string $table
	 * @return QueryBuilder
	 */
	public function innerJoin($table) {
		$this->queryFragments[] = new InnerJoinFragment($table);
		return $this;
	}

	/**appends the table declaration of an left-join
	 * @param string $table
	 * @return QueryBuilder
	 */
	public function leftJoin($table) {
		$this->queryFragments[] = new LeftJoinFragment($table);
		return $this;
	}

	/**
	 * appends the table declaration of an right-join
	 * @param string $table
	 * @return QueryBuilder
	 */
	public function rightJoin($table) {
		$this->queryFragments[] = new RightJoinFragment($table);
		return $this;
	}

	/**
	 * appends the table declaration of an full-join
	 * @param string $table
	 * @return QueryBuilder
	 */
	public function fullJoin($table) {
		$this->queryFragments[] = new FullJoinFragment($table);
		return $this;
	}

	/**
	 * appends the on declaration of a join statement
	 * @param string $lval
	 * @param string $rval
	 * @return QueryBuilder
	 */
	public function on($lval, $rval) {
		$this->queryFragments[] = new OnFragment($lval, $rval);
		return $this;
	}

	/**
	 * appends an on-check to the query
	 * @param string $lval
	 * @param array  $valueList
	 * @return QueryBuilder
	 */
	public function in($lval, array $valueList) {
		$this->queryFragments[] = new InFragment($lval, $valueList);
		return $this;
	}

	/**
	 * appends a like-check to the query
	 * @return QueryBuilder
	 */
	public function like() {
		$this->queryFragments[] = new LikeFragment();
		return $this;
	}

	/**
	 * appends a SQL function-call to the query
	 * @param string $funcName
	 * @param array  $args
	 * @return QueryBuilder
	 */
	public function functionCall($funcName, array $args) {
		$this->queryFragments[] = new FunctionCallFragment($funcName, $args);
		return $this;
	}

	/**
	 * @param string $funcName
	 * @param array $args
	 * @return FunctionCallFragment
	 */
	public function functionCallFragment($funcName, array $args) {
		return new FunctionCallFragment($funcName, $args);
	}

	/**
	 * append raw SQL to the query
	 * @param $rawSql
	 * @return QueryBuilder
	 */
	public function raw($rawSql) {
		$this->queryFragments[] = new RawFragment($rawSql);
		return $this;
	}

	/**
	 * bind parameters to the query
	 * @param mixed  $value
	 * @param string $type
	 * @return QueryBuilder
	 */
	public function bind($value, $type) {
		$this->boundParams[] = new BoundParameter($value, $type);
		return $this;
	}

	/**
	 * generate the SQL string
	 * @return string
	 */
	public function generate() {
		$stringList = $this->buildStringList();
		return implode(" ", $stringList);
	}

	/**
	 * generate the sql string and return the constructed Query object
	 * @return QueryInterface
	 */
	public function build() {
		$generatedQuery = $this->generate();
		$queryObj       = new Query($this->adapter, $generatedQuery);

		foreach ($this->boundParams as $param) {
			$queryObj->bind($param->getParam(), $param->getType());
		}

		return $queryObj;
	}

	/**
	 * generates an array of the SQL fragments to be joined together
	 * @return array
	 */
	private function buildStringList() {
		$buffer = [];

		foreach ($this->queryFragments as $fragment) {
			$buffer[] = $fragment->generate();
		}

		return $buffer;
	}
}