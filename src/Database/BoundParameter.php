<?php

namespace InSiteLogic\Database;

/**
 * Class BoundParameter
 *
 * @package InSiteLogic\Database
 */
class BoundParameter {

	/**
	 * @var mixed
	 */
	private $param;

	/**
	 * @var string
	 */
	private $type;

	/**
	 * BoundParameter constructor.
	 * @param mixed  $param
	 * @param string $type
	 */
	public function __construct($param, $type) {
		$this->param = $param;
		$this->type  = $type;
	}

	/**
	 * @return mixed
	 */
	public function getParam() {
		return $this->param;
	}

	/**
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}
}