<?php

namespace InSiteLogic\Database\Query;

/**
 * Interface QueryInterface
 *
 * @package InSiteLogic\Database\Query
 */
interface QueryInterface {

	/**
	 * bind a parameter to the query
	 * @param mixed  $value
	 * @param string $type
	 */
	public function bind($value, $type);

	/**
	 * fetch multiple results from the database
	 * @return array
	 */
	public function fetchMultiple();

	/**
	 * fetch a single result from the database
	 * @return array
	 */
	public function fetchSingle();

	/**
	 * execute a query on the database
	 * @return bool
	 */
	public function execute();

	/**
	 * get the last-updated id from the database
	 * @return int
	 */
	public function getLastUpdatedId();
}