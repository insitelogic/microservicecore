<?php

namespace InSiteLogic\Database\Query\Impl;

use InSiteLogic\Database\Query\QueryInterface;
use InSiteLogic\Util\ISDBAdapter;
use InSiteLogic\Util\ObjectHydrator;

/**
 * Class Query
 *
 * @package InSiteLogic\Database\Query\Impl
 */
class Query implements QueryInterface {

	/**
	 * @var ISDBAdapter
	 */
	private $dbAdapter;

	/**
	 * @var string
	 */
	private $queryString;

	/**
	 * @var array
	 */
	private $paramsList = [];

	/**
	 * @var array
	 */
	private $paramTypeList = [];

	/**
	 * Query constructor.
	 * @param ISDBAdapter $dbAdapter
	 * @param string      $queryString
	 */
	public function __construct(ISDBAdapter $dbAdapter, $queryString) {
		$this->dbAdapter   = $dbAdapter;
		$this->queryString = $queryString;
	}

	/**
	 * @param mixed $value
	 * @param string $type
	 */
	public function bind($value, $type) {
		$this->paramsList[] = $value;
		$this->paramTypeList[] = $type;
	}

	/**
	 * @return array
	 */
	public function fetchMultiple() {
		return $this->dbAdapter->queryDBForMultiple($this->queryString, $this->paramsList, $this->getTypeString(), false);
	}

	/**
	 * @param object $class
	 * @return object[]
	 */
	public function fetchHydratedMultiple($class) {
		$buffer = [];
		$data = $this->fetchMultiple();

		foreach ($data as $entry) {
			$buffer[] = ObjectHydrator::getDefaultHydrator()->hydrate($entry, $class);
		}

		return $buffer;
	}

	/**
	 * @return array
	 */
	public function fetchSingle() {
		$bindParams = count($this->paramsList) > 0 ? $this->paramsList : [];
		$bindTypeStr = count($this->paramsList) > 0 ? $this->getTypeString() : null;

		return $this->dbAdapter->queryDBForSingle($this->queryString, $bindParams, $bindTypeStr, false);
	}

	/**
	 * @param object $class
	 * @return object
	 */
	public function fetchHydratedSingle($class) {
		$data = $this->fetchSingle();
		return ObjectHydrator::getDefaultHydrator()->hydrate($data, $class);
	}

	/**
	 * @return bool
	 */
	public function execute() {
		return $this->dbAdapter->runUpdateQuery($this->queryString, $this->paramsList, $this->getTypeString());
	}

	/**
	 * @return int
	 */
	public function getLastUpdatedId() {
		return $this->dbAdapter->getLastUpdatedId();
	}

	/**
	 * @return string
	 */
	private function getTypeString() {
		return implode("", $this->paramTypeList);
	}
}