<?php

namespace InSiteLogic\Database\Provider;

use InSiteLogic\Util\ISDBAdapter;
use mysqli;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class DatabaseAdapterServiceProvider implements ServiceProviderInterface {

	/**
	 * Registers services on the given container.
	 * This method should only be used to configure services and parameters.
	 * It should not get services.
	 * @param Container $pimple A container instance
	 */
	public function register(Container $pimple) {
		//mysql default service
		$pimple['db.mysqli'] = function (Container $pimple) {
			/** @var \InSiteLogic\Identity\Identity $identity */
			$identity = $pimple['identity'];
			syslog(LOG_INFO, print_r($identity, true));

			if ($_SERVER['SERVER_NAME'] == 'localhost') {
				syslog(LOG_INFO, "we are in testing mode");
				return new mysqli($identity->getDbHost(), $identity->getDbUser(), $identity->getDbPass(), $identity->getDbName());
			} else {
				syslog(LOG_INFO, "we are in production mode");
				return new mysqli(null, $identity->getDbUser(), $identity->getDbPass(), $identity->getDbName(), null, $identity->getDbSock());
			}
		};

		$pimple['db.mysqli.read'] = function (Container $pimple) {
			/** @var \InSiteLogic\Identity\Identity $identity */
			$identity = $pimple['identity'];

			if($identity->hasReadReplica()) {
				syslog(LOG_INFO, "identity has a read replica configuration set.");
				$readReplica = $identity->getReadReplicaConfiguration();
				if ($_SERVER['SERVER_NAME'] == 'localhost') {
					syslog(LOG_INFO, "we are in testing mode");
					return new mysqli($readReplica['db_host'], $readReplica['db_user'], $readReplica['db_pass'], $readReplica['db_name']);
				} else {
					syslog(LOG_INFO, "we are in production mode");
					return new mysqli(null, $readReplica['db_user'], $readReplica['db_pass'], $readReplica['db_name'], null, $readReplica['db_sock']);
				}
			} else {
				syslog(LOG_INFO, "no read replica available, use single connection only.");
				return false;
			}
		};

		//database wrapper service
		$pimple['db'] = function (Container $pimple) {
			if($pimple['db.mysqli.read'] == false) {
				syslog(LOG_INFO, "creating adapter with shared read/write connection.");
				return new ISDBAdapter($pimple['db.mysqli']);
			} else {
				syslog(LOG_INFO, "creating adapter with seperate read/write connection.");
				return new ISDBAdapter($pimple['db.mysqli'], $pimple['db.mysqli.read']);
			}
		};
	}
}