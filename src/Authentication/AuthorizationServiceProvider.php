<?php

namespace InSiteLogic\Authentication;

use Exception;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Api\BootableProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class AuthorizationServiceProvider implements ServiceProviderInterface, BootableProviderInterface {

	/**
	 * Bootstraps the application.
	 * This method is called after all services are registered
	 * and should be used for "dynamic" configuration (whenever
	 * a service must be requested).
	 * @param Application $app
	 */
	public function boot(Application $app) {
		//get authentication user
		$app->before(function (Request $request, $app) {
			if(strtolower($request->getMethod()) !== "options" && !$request->headers->has("x-appengine-queuename")) {
				syslog(LOG_INFO, "performing authentication check");
				$token    = $request->headers->get('IS-Token');
				$identity = $request->headers->get('IS-Identity');
				$user     = AuthenticationProvider::createProvider($app['authorization.service'])->request($token, $identity);

				if ($user === null) {
					syslog(LOG_ERR, "user is null from AuthenticationProvider");
					throw new Exception('Invalid authentication token.');
				}

				$app['user'] = $user;
			}
		});
	}

	/**
	 * Registers services on the given container.
	 * This method should only be used to configure services and parameters.
	 * It should not get services.
	 * @param Container $pimple A container instance
	 */
	public function register(Container $pimple) {}
}