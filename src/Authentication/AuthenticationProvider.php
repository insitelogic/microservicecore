<?php

namespace InSiteLogic\Authentication;

use GuzzleHttp\Client;
use InSiteLogic\Util\ObjectHydrator;
use Zend\Hydrator\Reflection;

class AuthenticationProvider {

	const IDENTITY_URL_PATH = "/is-valid";

	/**
	 * @var Client
	 */
	private $client;

	/**
	 * @var string
	 */
	private $baseUrl;

	/**
	 * @param string $baseUrl
	 * @return AuthenticationProvider
	 */
	public static function createProvider($baseUrl) {
		return new AuthenticationProvider($baseUrl);
	}

	/**
	 * IdentityProvider constructor.
	 *
	 * @param string $baseUrl
	 */
	public function __construct($baseUrl) {
		$this->baseUrl = $baseUrl;

		syslog(LOG_INFO, "Constructing AuthenticationProvider with base_uri of $baseUrl");

		$this->client = new Client(
			[
				'base_uri' => $this->baseUrl
			]
		);
	}

	/**
	 * @param string $token
	 * @param string $isIdentity
	 * @return AuthUser
	 */
	public function request($token, $isIdentity) {
		syslog(LOG_INFO, "Access token is: $token");
		syslog(LOG_INFO, "Identity is: $isIdentity");

		syslog(LOG_INFO, "making authentication request");
		$json = $this->client->post(
			$this->createRequestPath(), [
			'allow_redirects' => false,
			'json' => [
				'token' => $token
			],
			'headers' => [
				'IS-Identity' => $isIdentity
			]
		]);

		syslog(LOG_INFO, "made authentication request, now processing contents.");
		$payload = json_decode($json->getBody()->getContents(), true);
		$data = $payload['data'];

		syslog(LOG_INFO, print_r($data, true));

		if($data !== null) {
			syslog(LOG_INFO, "make user object");
			$user = ObjectHydrator::getDefaultHydrator()->hydrate($data, AuthUser::class);

			$projectsList = [];
			syslog(LOG_INFO, "make projects array");
			foreach($data['projects'] as $project) {
				$p = ObjectHydrator::getDefaultHydrator()->hydrate($project, Project::class);
				$projectsList[] = $p;
			}

			$rolesList = [];
			syslog(LOG_INFO, "make roles array");
			foreach ($data['roles'] as $role) {
				$r = ObjectHydrator::getDefaultHydrator()->hydrate($role, UserRole::class);
				$rolesList[] = $r;
			}

			syslog(LOG_INFO, "make object tree");
			$user->setProjects($projectsList);
			$user->setRoles($rolesList);

			syslog(LOG_INFO, "return new user");
			return $user;
		}

		syslog(LOG_WARNING, "no user to return, return null");
		return null;
	}

	private function createRequestPath() {
		return AuthenticationProvider::IDENTITY_URL_PATH;
	}
}