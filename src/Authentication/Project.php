<?php

namespace InSiteLogic\Authentication;

class Project implements \JsonSerializable {

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $projectName;

	/**
	 * @var string
	 */
	private $projectSku;

	/**
	 * @var string
	 */
	private $projectLogo;

	/**
	 * @var string
	 */
	private $projectLaunchDate;

	/**
	 * @var boolean
	 */
	private $enabled;

	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id) {
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getProjectName() {
		return $this->projectName;
	}

	/**
	 * @param string $projectName
	 */
	public function setProjectName($projectName) {
		$this->projectName = $projectName;
	}

	/**
	 * @return string
	 */
	public function getProjectSku() {
		return $this->projectSku;
	}

	/**
	 * @param string $projectSku
	 */
	public function setProjectSku($projectSku) {
		$this->projectSku = $projectSku;
	}

	/**
	 * @return string
	 */
	public function getProjectLogo() {
		return $this->projectLogo;
	}

	/**
	 * @param string $projectLogo
	 */
	public function setProjectLogo($projectLogo) {
		$this->projectLogo = $projectLogo;
	}

	/**
	 * @return string
	 */
	public function getProjectLaunchDate() {
		return $this->projectLaunchDate;
	}

	/**
	 * @param string $projectLaunchDate
	 */
	public function setProjectLaunchDate($projectLaunchDate) {
		$this->projectLaunchDate = $projectLaunchDate;
	}

	/**
	 * @return boolean
	 */
	public function isEnabled() {
		return $this->enabled;
	}

	/**
	 * @param boolean $enabled
	 */
	public function setEnabled($enabled) {
		$this->enabled = $enabled;
	}

	/**
	 * Specify data which should be serialized to JSON
	 * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 * which is a value of any type other than a resource.
	 * @since 5.4.0
	 */
	function jsonSerialize() {
		return [
			'id' => $this->getId(),
			'projectName' => $this->getProjectName(),
			'projectSku' => $this->getProjectSku(),
			'projectLogo' => $this->getProjectLogo(),
			'projectLaunchDate' => $this->getProjectLaunchDate(),
			'enabled' => $this->isEnabled()
		];
	}
}