<?php

namespace InSiteLogic\Authentication;

class AuthUser implements \JsonSerializable {
	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $email;

	/**
	 * @var string
	 */
	private $username;

	/**
	 * @var string
	 */
	private $password;

	/**
	 * @var string
	 */
	private $firstName;

	/**
	 * @var string
	 */
	private $lastName;

	/**
	 * @var string
	 */
	private $phone;

	/**
	 * @var string
	 */
	private $token;

	/**
	 * @var boolean
	 */
	private $enabled;

	/**
	 * @var Project[]
	 */
	private $projects;

	/**
	 * @var UserRole[]
	 */
	private $roles;

	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id) {
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @param string $email
	 */
	public function setEmail($email) {
		$this->email = $email;
	}

	/**
	 * @return string
	 */
	public function getUsername() {
		return $this->username;
	}

	/**
	 * @param string $username
	 */
	public function setUsername($username) {
		$this->username = $username;
	}

	/**
	 * @return string
	 */
	public function getPassword() {
		return $this->password;
	}

	/**
	 * @param string $password
	 */
	public function setPassword($password) {
		$this->password = $password;
	}

	/**
	 * @return string
	 */
	public function getFirstName() {
		return $this->firstName;
	}

	/**
	 * @param string $firstName
	 */
	public function setFirstName($firstName) {
		$this->firstName = $firstName;
	}

	/**
	 * @return string
	 */
	public function getLastName() {
		return $this->lastName;
	}

	/**
	 * @param string $lastName
	 */
	public function setLastName($lastName) {
		$this->lastName = $lastName;
	}

	/**
	 * @return string
	 */
	public function getPhone() {
		return $this->phone;
	}

	/**
	 * @param string $phone
	 */
	public function setPhone($phone) {
		$this->phone = $phone;
	}

	/**
	 * @return string
	 */
	public function getToken() {
		return $this->token;
	}

	/**
	 * @param string $token
	 */
	public function setToken($token) {
		$this->token = $token;
	}

	/**
	 * @return boolean
	 */
	public function isEnabled() {
		return $this->enabled;
	}

	/**
	 * @param boolean $enabled
	 */
	public function setEnabled($enabled) {
		$this->enabled = $enabled;
	}

	/**
	 * @return Project[]
	 */
	public function getProjects() {
		return $this->projects;
	}

	/**
	 * @param Project[] $projects
	 */
	public function setProjects($projects) {
		$this->projects = $projects;
	}

	/**
	 * @return UserRole[]
	 */
	public function getRoles() {
		return $this->roles;
	}

	/**
	 * @param UserRole[] $roles
	 */
	public function setRoles($roles) {
		$this->roles = $roles;
	}

	/**
	 * @return array
	 */
	private function getSerializableProjects() {
		$buffer = [];

		foreach ($this->getProjects() as $project) {
			$buffer[] = $project->jsonSerialize();
		}

		return $buffer;
	}

	/**
	 * @return array
	 */
	private function getSerializableRoles() {
		$buffer = [];

		foreach ($this->getRoles() as $role) {
			$buffer[] = $role->jsonSerialize();
		}

		return $buffer;
	}

	/**
	 * Specify data which should be serialized to JSON
	 * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 * which is a value of any type other than a resource.
	 * @since 5.4.0
	 */
	function jsonSerialize() {
		return [
			'id' => $this->getId(),
			'email' => $this->getEmail(),
			'username' => $this->getUsername(),
			'firstName' => $this->getFirstName(),
			'lastName' => $this->getLastName(),
			'phone' => $this->getPhone(),
			'token' => $this->getToken(),
			'enabled' => $this->isEnabled(),
			'projects' => $this->getSerializableProjects(),
			'roles' => $this->getSerializableRoles()
		];
	}
}