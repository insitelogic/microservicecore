<?php

namespace InSiteLogic\Authentication;

class UserRole implements \JsonSerializable {

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $roleName;

	/**
	 * @var string
	 */
	private $permissions;

	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id) {
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getRoleName() {
		return $this->roleName;
	}

	/**
	 * @param string $roleName
	 */
	public function setRoleName($roleName) {
		$this->roleName = $roleName;
	}

	/**
	 * @return string
	 */
	public function getPermissions() {
		return $this->permissions;
	}

	/**
	 * @param string $permissions
	 */
	public function setPermissions($permissions) {
		$this->permissions = $permissions;
	}

	public function getPermissionsArray() {
		$result = [];
		if(!empty($this->permissions) && $this->permissions !== '*') {
			$result = json_decode($this->permissions, true);
		}

		return $result;
	}

	/**
	 * Specify data which should be serialized to JSON
	 * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 * which is a value of any type other than a resource.
	 * @since 5.4.0
	 */
	function jsonSerialize() {
		return [
			'id' => $this->getId(),
			'roleName' => $this->getRoleName(),
			'permissions' => $this->getPermissions()
		];
	}
}