<?php

namespace InSiteLogic\MicroService\Response;

use JsonSerializable;
use Symfony\Component\HttpFoundation\JsonResponse;

class GenericMicroServiceResponse extends JsonResponse {
	/**
	 * GenericMicroServiceResponse constructor.
	 * @param JsonSerializable|array $data
	 */
	public function __construct($data) {
		parent::__construct();

		$d = [];
		if($data instanceof JsonSerializable) {
			$d = $data->jsonSerialize();
		} else if($this->paramIsArrayOfJsonSerializableObjects($data)) {
			foreach ($data as $record) {
				$d[] = $record->jsonSerialize();
			}
		} else if($d !== null) {
			$d = $data;
		}

		$this->setData([
			'metadata' => [
				'hostname'    => getenv('HTTP_HOST'),
				'path'        => getenv('PATH_INFO'),
				'instanceId'  => getenv('INSTANCE_ID'),
				'codeVersion' => getenv('CURRENT_VERSION_ID'),
				'requestId'   => getenv('REQUEST_LOG_ID')
			],
			'data'     => $d
		]);
	}

	/**
	 * @param $data
	 * @return bool
	 */
	private function paramIsArrayOfJsonSerializableObjects($data) {
	    if(count($data) <= 100) {
            syslog(LOG_INFO, "data variable is as follows: ");
            syslog(LOG_INFO, print_r($data, true));
        } else {
            syslog(LOG_INFO, "data set is >100 rows, skipping syslog output...");
        }

		if(is_array($data)) {
			syslog(LOG_INFO, "data is an array");
			if(count($data) > 0) {
				syslog(LOG_INFO, "array length is greater than 0");
				$indexed = array_values($data);

				syslog(LOG_INFO, "force 0-indexed to ensure no index errors");

				if($indexed[0] instanceof JsonSerializable) {
					return true;
				}
			}
		}

		return false;
	}
}