<?php

namespace InSiteLogic\MicroService\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

abstract class AbstractMicroServiceController {

	/**
	 * @param mixed $data
	 * @return JsonResponse
	 */
	protected function asJson($data) {
		return new JsonResponse($data);
	}
}