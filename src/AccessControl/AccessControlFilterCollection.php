<?php

namespace InSiteLogic\AccessControl;

use InSiteLogic\AccessControl\Filter\AccessControlFilter;

class AccessControlFilterCollection {

	/**
	 * @var AccessControlFilter[]
	 */
	private $filterCollection;

	/**
	 * AccessControlFilterCollection constructor
	 */
	public function __construct() {
		$this->filterCollection = [];
	}

	/**
	 * @param AccessControlFilter $filter
	 */
	public function add(AccessControlFilter $filter) {
		$this->filterCollection[] = $filter;
	}

	/**
	 * @param $filterKey
	 * @return AccessControlFilter
	 */
	public function get($filterKey) {
		$result = null;

		foreach ($this->filterCollection as $filter) {
			if($filter->getFilterKey() === $filterKey) {
				$result = $filter;
				break;
			}
		}

		return $result;
	}

	/**
	 * @param $data
	 * @return array
	 */
	public function apply($data) {
		$ptr = $data;

		foreach ($this->filterCollection as $filter) {
			$ptr = $filter->filter($ptr);
		}

		return $ptr;
	}
}