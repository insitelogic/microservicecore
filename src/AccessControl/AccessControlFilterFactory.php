<?php

namespace InSiteLogic\AccessControl;

use InSiteLogic\AccessControl\Filter\AccessControlFilter;
use InSiteLogic\AccessControl\Filter\Impl\SalesAgentAssignedLeadsFilter;
use InSiteLogic\Authentication\AuthUser;
use InSiteLogic\Util\ISDBAdapter;

class AccessControlFilterFactory {

	/**
	 * @var AuthUser
	 */
	private $currentUser;

	/**
	 * @var ISDBAdapter
	 */
	private $dbAdapter;

	/**
	 * AccessControlFilterFactory constructor.
	 * @param AuthUser    $currentUser
	 * @param ISDBAdapter $dbAdapter
	 */
	public function __construct(AuthUser $currentUser, ISDBAdapter $dbAdapter=null) {
		$this->currentUser = $currentUser;
		$this->dbAdapter   = $dbAdapter;
	}

	/**
	 * @return AccessControlFilterCollection
	 */
	public function build() {
		$filterCollection = new AccessControlFilterCollection();
		if($this->currentUser->getRoles() !== null && count($this->currentUser->getRoles()) > 0) {
			foreach ($this->currentUser->getRoles() as $role) {
				$roleKeyList = $role->getPermissionsArray();

				foreach ($roleKeyList as $roleKey) {
					$filter = $this->createFilter($this->currentUser, $this->dbAdapter, $roleKey);
					if($filter !== null) {
						$filterCollection->add($filter);
					}
				}
			}
		}

		return $filterCollection;
	}

	/**
	 * @param AuthUser    $currentUser
	 * @param ISDBAdapter $adapter
	 * @param string      $filterKey
	 * @return AccessControlFilter
	 */
	public function createFilter(AuthUser $currentUser, ISDBAdapter $adapter, $filterKey) {
		switch ($filterKey) {
			case 'SALES_AGENT_ASSIGNED_ONLY':
				return new SalesAgentAssignedLeadsFilter($currentUser, $adapter);
			default:
				return null;
		}
	}
}