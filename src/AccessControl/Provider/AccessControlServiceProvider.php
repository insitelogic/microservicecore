<?php

namespace InSiteLogic\AccessControl\Provider;

use InSiteLogic\AccessControl\AccessControlFilterFactory;
use InSiteLogic\Authentication\AuthUser;
use InSiteLogic\Util\ISDBAdapter;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Api\BootableProviderInterface;
use Silex\Application;

class AccessControlServiceProvider implements ServiceProviderInterface, BootableProviderInterface {

	/**
	 * Registers services on the given container.
	 * This method should only be used to configure services and parameters.
	 * It should not get services.
	 * @param Container $pimple A container instance
	 */
	public function register(Container $pimple) { }

	/**
	 * Bootstraps the application.
	 * This method is called after all services are registered
	 * and should be used for "dynamic" configuration (whenever
	 * a service must be requested).
	 * @param Application $app
	 */
	public function boot(Application $app) {
		$app['access-control-factory'] = function (Application $app) {
			/** @var AuthUser $currentUser */
			$currentUser = $app['user'];
			/** @var ISDBAdapter $dbAdapter */
			$dbAdapter = $app['db'];

			return new AccessControlFilterFactory($currentUser, $dbAdapter);
		};
	}
}