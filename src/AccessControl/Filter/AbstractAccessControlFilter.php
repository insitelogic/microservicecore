<?php

namespace InSiteLogic\AccessControl\Filter;

use InSiteLogic\Authentication\AuthUser;
use InSiteLogic\Util\ISDBAdapter;

abstract class AbstractAccessControlFilter implements AccessControlFilter {

	/**
	 * @var AuthUser
	 */
	protected $currentUser;

	/**
	 * @var ISDBAdapter
	 */
	private $dbAdapter;

	/**
	 * AbstractAccessControlFilter constructor.
	 * @param AuthUser         $currentUser
	 * @param ISDBAdapter|null $adapter
	 */
	public function __construct(AuthUser $currentUser, ISDBAdapter $adapter = null) {
		$this->currentUser = $currentUser;
		$this->dbAdapter = $adapter;
	}

	/**
	 * @return string
	 */
	public function getFilterKey() {
		return $this->requiredPermission();
	}

	/**
	 * @return string
	 */
	protected abstract function requiredPermission();

	/**
	 * @return bool
	 */
	protected function hasDatabaseAdapter() {
		return $this->dbAdapter !== null;
	}

	/**
	 * @return ISDBAdapter|null
	 */
	protected function getDatabaseAdapter() {
		return $this->dbAdapter;
	}
}