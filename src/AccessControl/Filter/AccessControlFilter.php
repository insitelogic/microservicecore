<?php

namespace InSiteLogic\AccessControl\Filter;

use InSiteLogic\Authentication\UserRole;

interface AccessControlFilter {

	/**
	 *
	 * @param $data
	 * @return mixed
	 */
	public function filter($data);

	/**
	 * @param UserRole $role
	 * @return bool
	 */
	public function shouldApplyFilter(UserRole $role);

	public function getFilterKey();
}