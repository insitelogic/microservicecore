<?php

namespace InSiteLogic\AccessControl\Filter\Impl;

use InSiteLogic\AccessControl\Filter\AbstractAccessControlFilter;
use InSiteLogic\Authentication\UserRole;

class SalesAgentAssignedLeadsFilter extends AbstractAccessControlFilter {

	const REQUIRED_PERMISSION = "SALES_AGENT_ASSIGNED_ONLY";

	/**
	 * @param $data
	 * @return mixed
	 */
	/**
	 * @param $data
	 * @return mixed
	 */
	public function filter($data) {
		$buffer = [];

		$myId = $this->currentUser->getId();
		foreach ($data as $datum) {
			if(method_exists($datum, 'getAgentId')) {
				$agentId = $datum->getAgentId();
				if($agentId == $myId) {
					$buffer[] = $datum;
				}
			} else if(is_array($datum)) {
				if(array_key_exists('agentId', $datum) && $datum['agentId'] === $myId) {
					$buffer[] = $datum;
				} else if(array_key_exists('agentid', $datum) && $datum['agentid'] === $myId) {
					$buffer[] = $datum;
				} else if(array_key_exists('agent_id', $datum) && $datum['agent_id'] === $myId) {
					$buffer[] = $datum;
				}
			}
		}

		return $buffer;
	}

	/**
	 * @param UserRole $role
	 * @return bool
	 */
	public function shouldApplyFilter(UserRole $role) {
		$shouldApplyFilter = false;

		$permissionsList = $role->getPermissionsArray();
		if(in_array($this->requiredPermission(), $permissionsList)) {
			return true;
		}

		return $shouldApplyFilter;
	}

	protected function requiredPermission() {
		return SalesAgentAssignedLeadsFilter::REQUIRED_PERMISSION;
	}
}