<?php

namespace InSiteLogic\Http;

use InSiteLogic\MicroService\Response\GenericMicroServiceResponse;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Api\BootableProviderInterface;
use Silex\Application;

class GenericApiErrorServiceProvider implements ServiceProviderInterface, BootableProviderInterface {

	/**
	 * Bootstraps the application.
	 * This method is called after all services are registered
	 * and should be used for "dynamic" configuration (whenever
	 * a service must be requested).
	 * @param Application $app
	 */
	public function boot(Application $app) {
		//default error handler
		$app->error(function (\Exception $e, $code) {
			syslog(LOG_ERR, "We received an error! Returning error response");
			syslog(LOG_ERR, $e->getMessage());
			syslog(LOG_ERR, $e->getTraceAsString());
			return new GenericMicroServiceResponse([
				'errorMessage' => $e->getMessage(),
				'errorCode' => $e->getCode()
			]);
		});
	}

	/**
	 * Registers services on the given container.
	 * This method should only be used to configure services and parameters.
	 * It should not get services.
	 * @param Container $pimple A container instance
	 */
	public function register(Container $pimple) {}
}