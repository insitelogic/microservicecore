<?php

namespace InSiteLogic\Http;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Api\BootableProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class JsonBodyParserServiceProvider implements ServiceProviderInterface, BootableProviderInterface {

	/**
	 * Bootstraps the application.
	 * This method is called after all services are registered
	 * and should be used for "dynamic" configuration (whenever
	 * a service must be requested).
	 * @param Application $app
	 */
	public function boot(Application $app) {
		//parse json request body
		$app->before(function (Request $request) {
			syslog(LOG_INFO, "checking if we need to process JSON body");
			if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
				syslog(LOG_INFO, "converting JSON...");
				$data = json_decode($request->getContent(), true);
				$request->request->replace(is_array($data) ? $data : array());
			}
		});
	}

	/**
	 * Registers services on the given container.
	 * This method should only be used to configure services and parameters.
	 * It should not get services.
	 * @param Container $pimple A container instance
	 */
	public function register(Container $pimple) {}
}