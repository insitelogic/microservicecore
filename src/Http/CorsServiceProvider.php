<?php

namespace InSiteLogic\Http;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Api\BootableProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CorsServiceProvider implements ServiceProviderInterface, BootableProviderInterface {

	/**
	 * Bootstraps the application.
	 * This method is called after all services are registered
	 * and should be used for "dynamic" configuration (whenever
	 * a service must be requested).
	 * @param Application $app
	 */
	public function boot(Application $app) {
		$app->options("{anything}", function () { return new Response(); })->assert("anything", ".*");

		//set Access-Control-Allow-Origin to anything on response
		$app->after(function (Request $request, Response $response) {
			syslog(LOG_INFO, print_r($request->headers->all(), true));

			$origin = "*";
			$origin = array_key_exists('HTTP_ORIGIN', $_SERVER) ? $_SERVER['HTTP_ORIGIN'] : $origin;

			$response->headers->set('P3P', 'CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
			$response->headers->set('Access-Control-Allow-Method', 'GET, POST, PUT, DELETE, OPTIONS, HEAD');
			$response->headers->set('Access-Control-Allow-Headers', 'X-Requested-With, Accept, Accept-Language, Content-Language, Content-Type, content-type, IS-Token, IS-Identity');
			$response->headers->set('Access-Control-Allow-Origin', $origin);
			$response->headers->set('Access-Control-Allow-Credentials', 'true');
			syslog(LOG_INFO, "response headers are as follows: ");
			syslog(LOG_INFO, print_r($response->headers->all(), true));
		});
	}

	/**
	 * Registers services on the given container.
	 * This method should only be used to configure services and parameters.
	 * It should not get services.
	 * @param Container $pimple A container instance
	 */
	public function register(Container $pimple) {}
}