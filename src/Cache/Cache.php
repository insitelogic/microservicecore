<?php

namespace InSiteLogic\Cache;

interface Cache {

	/**
	 * @param string $key
	 * @param mixed $value
	 * @return bool
	 */
	public function add($key, $value);

	/**
	 * @param string $key
	 * @return bool
	 */
	public function delete($key);

	/**
	 * @return bool
	 */
	public function flush();

	/**
	 * @param string $key
	 * @return mixed
	 */
	public function get($key);

	/**
	 * @param string $key
	 * @param mixed $value
	 * @return mixed
	 */
	public function replace($key, $value);

	/**
	 * @param string $key
	 * @param mixed $value
	 * @return bool
	 */
	public function set($key, $value);

	/**
	 * @param string $key
	 * @return bool
	 */
	public function has($key);
}