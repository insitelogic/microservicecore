<?php

namespace InSiteLogic\Cache;

class CacheFactory {

	/**
	 * @var array
	 */
	private static $store = [];

	/**
	 * @param string $instanceClass
	 * @return Cache
	 */
	public static function getInstance($instanceClass) {
		if(!array_key_exists($instanceClass, CacheFactory::$store)) {
			CacheFactory::$store[$instanceClass] = new $instanceClass();
		}

		return CacheFactory::$store[$instanceClass];
	}

	/**
	 * @param string $string,...
	 * @return string
	 */
	public static function generateCacheKey($string = "") {
		$accum = $string;

		foreach(func_get_args() as $arg) {
			$tmp = strval($arg);
			$accum .= $tmp;
		}

		return password_hash($accum, PASSWORD_BCRYPT);
	}
}