<?php

namespace InSiteLogic\Cache\Impl;

use InSiteLogic\Cache\Cache;
use Memcache;

class AppEngineCache implements Cache {

	/**
	 * @var Memcache
	 */
	private $cache;

	/**
	 * AppEngineCache constructor.
	 */
	public function __construct() {
		$this->cache = new Memcache();
	}

	/**
	 * @param string $key
	 * @param mixed  $value
	 * @return bool
	 */
	public function add($key, $value) {
		return $this->cache->add($key, $value);
	}

	/**
	 * @param string $key
	 * @return bool
	 */
	public function delete($key) {
		return $this->cache->delete($key);
	}

	/**
	 * @return bool
	 */
	public function flush() {
		return $this->cache->flush();
	}

	/**
	 * @param array|string $key
	 * @return mixed
	 */
	public function get($key) {
		return $this->cache->get($key);
	}

	/**
	 * @param string $key
	 * @param mixed  $value
	 * @return mixed
	 */
	public function replace($key, $value) {
		return $this->cache->replace($key, $value);
	}

	/**
	 * @param string $key
	 * @param mixed  $value
	 * @return bool
	 */
	public function set($key, $value) {
		return $this->cache->set($key, $value);
	}

	/**
	 * @param string $key
	 * @return bool
	 */
	public function has($key) {
		$getResult = $this->get($key);
		if($getResult === false) {
			return false;
		} else {
			return true;
		}
	}
}