<?php

namespace InSiteLogic\Messaging\Provider;

use Google\Cloud\PubSub\PubSubClient;
use InSiteLogic\Messaging\Facade\InSiteMessagingFacade;
use InSiteLogic\Messaging\Gateway\GCloudPubSubGateway;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Application;

class CloudMessagingProvider implements ServiceProviderInterface {

	/**
	 * Registers services on the given container.
	 *
	 * This method should only be used to configure services and parameters.
	 * It should not get services.
	 *
	 * @param Container $app A container instance
	 */
	public function register(Container $app) {
		$app['gcloud.messaging.options'] = [
			'keyfile' => null
		];

		$app['gcloud.messaging.client'] = function (Application $app) {
			$pubSubClient = new PubSubClient([
				'keyFilePath' => $app['gcloud.messaging.options']['keyfile']
			]);

			return $pubSubClient;
		};

		$app['gcloud.messaging.gateway'] = function (Application $app) {
			return GCloudPubSubGateway::createGateway($app['gcloud.messaging.client']);
		};

		$app['gcloud.messaging'] = function (Application $app) {
			return new InSiteMessagingFacade($app['gcloud.messaging.gateway'], $app['identity']->getId());
		};
	}
}