<?php

namespace InSiteLogic\Messaging\Facade;

use InSiteLogic\Messaging\Gateway\GCloudPubSubGateway;
use InSiteLogic\Messaging\Model\InSiteMessage;

class InSiteMessagingFacade {

	const PUBLISH_USER_CREATED = 'user_created';

	/**
	 * @var GCloudPubSubGateway
	 */
	private $pubSubGateway;

	/**
	 * @var int
	 */
	private $identity;

	/**
	 * InSiteMessagingFacade constructor.
	 * @param GCloudPubSubGateway $pubSubGateway
	 * @param int $identity
	 */
	public function __construct(GCloudPubSubGateway $pubSubGateway, $identity) {
		$this->pubSubGateway = $pubSubGateway;
		$this->identity = $identity;
	}

	public function publish(InSiteMessage $message) {
		if($message->getIdentity() === null) {
			$message->setIdentity($this->identity);
		}

		return $this->pubSubGateway->publish($message->asGCloudMessage());
	}
}