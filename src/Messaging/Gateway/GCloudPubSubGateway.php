<?php

namespace InSiteLogic\Messaging\Gateway;

use Google\Cloud\PubSub\PubSubClient;
use InSiteLogic\Messaging\Model\GCloudMessage;

class GCloudPubSubGateway {

	/**
	 * @var PubSubClient
	 */
	private $pubSubClient;

	/**
	 * @param PubSubClient $pubSubClient
	 * @return GCloudPubSubGateway
	 */
	public static function createGateway(PubSubClient $pubSubClient) {
		return new GCloudPubSubGateway($pubSubClient);
	}

	/**
	 * GCloudPubSubGateway constructor.
	 * @param PubSubClient $pubSubClient
	 */
	private function __construct(PubSubClient $pubSubClient) { $this->pubSubClient = $pubSubClient; }

	/**
	 * @param GCloudMessage $message
	 * @return null|mixed
	 */
	public function publish(GCloudMessage $message) {
		$result = null;
		if($message !== null && $message->getTopic() !== null) {
			$result = $this->pubSubClient->topic($message->getTopic())->publish($message->asPayload());
		}

		return $result;
	}

	/**
	 * @param string $topic
	 * @param string $subscriptionName
	 * @param mixed $subscriptionOpts
	 */
	public function subscribe($topic, $subscriptionName, $subscriptionOpts) {
		$subscription = $this->pubSubClient->topic($topic)->subscribe($subscriptionName);
		$subscription->create($subscriptionOpts);
	}

	public function __instantiateForTesting() {
		$topic = $this->pubSubClient->createTopic('create_user');
		$this->subscribe('create_user', 'test_sub', [ 'pushEndpoint' => 'http://128.199.138.56/oh/post_dump.php' ]);
	}
}