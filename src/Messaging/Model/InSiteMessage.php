<?php

namespace InSiteLogic\Messaging\Model;

class InSiteMessage {

	/**
	 * @var string
	 */
	private $topic;

	/**
	 * @var mixed
	 */
	private $payload;

	/**
	 * @var string
	 */
	private $messageSource;

	/**
	 * @var int
	 */
	private $identity;

	/**
	 * @var string
	 */
	private $projectId;

	/**
	 * @param string $topic
	 * @param int $identity
	 * @param mixed $payload
	 * @param string $projectSpecifier
	 * @return InSiteMessage
	 */
	public static function create($topic, $identity, $payload, $projectSpecifier) {
		$msg = new InSiteMessage();
		$msg->setTopic($topic);
		$msg->setPayload($payload);
		$msg->setIdentity($identity);
		$msg->setProjectId($projectSpecifier);
		$msg->setMessageSource($topic);

		return $msg;
	}

	/**
	 * @return string
	 */
	public function getTopic() {
		return $this->topic;
	}

	/**
	 * @param string $topic
	 */
	public function setTopic($topic) {
		$this->topic = $topic;
	}

	/**
	 * @return mixed
	 */
	public function getPayload() {
		return $this->payload;
	}

	/**
	 * @param mixed $payload
	 */
	public function setPayload($payload) {
		$this->payload = $payload;
	}

	/**
	 * @return string
	 */
	public function getMessageSource() {
		return $this->messageSource;
	}

	/**
	 * @param string $messageSource
	 */
	public function setMessageSource($messageSource) {
		$this->messageSource = $messageSource;
	}

	/**
	 * @return int
	 */
	public function getIdentity() {
		return $this->identity;
	}

	/**
	 * @param int $identity
	 */
	public function setIdentity($identity) {
		$this->identity = $identity;
	}

	/**
	 * @return string
	 */
	public function getProjectId() {
		return $this->projectId;
	}

	/**
	 * @param string $projectId
	 */
	public function setProjectId($projectId) {
		$this->projectId = $projectId;
	}

	/**
	 * @return GCloudMessage
	 */
	public function asGCloudMessage() {
		$msg = new GCloudMessage();
		$msg->setPayload($this->payload);
		$msg->setTopic($this->topic);
		$msg->setMetadata([
			'identity' => strval($this->identity),
			'project_id' => strval($this->projectId),
			'source' => strval($this->messageSource)
		]);

		return $msg;
	}
}