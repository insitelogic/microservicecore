<?php

namespace InSiteLogic\Messaging\Model;

class GCloudMessage {

	/**
	 * @var string
	 */
	private $topic;

	/**
	 * @var mixed
	 */
	private $payload;

	/**
	 * @var array
	 */
	private $metadata;

	/**
	 * @return string
	 */
	public function getTopic() {
		return $this->topic;
	}

	/**
	 * @param string $topic
	 */
	public function setTopic($topic) {
		$this->topic = $topic;
	}

	/**
	 * @return mixed
	 */
	public function getPayload() {
		return $this->payload;
	}

	/**
	 * @param mixed $payload
	 */
	public function setPayload($payload) {
		$this->payload = $payload;
	}

	/**
	 * @return array
	 */
	public function getMetadata() {
		return $this->metadata;
	}

	/**
	 * @param array $metadata
	 */
	public function setMetadata($metadata) {
		$this->metadata = $metadata;
	}

	/**
	 * @return array
	 */
	public function asPayload() {
		$payload = [ 'data' => json_encode($this->payload) ];
		if($this->metadata !== null) {
			$payload['attributes'] = $this->metadata;
		}

		return $payload;
	}
}