<?php

namespace InSiteLogic\Event\Provider;

use InSiteLogic\Event\EventBus\Impl\GoogleTaskQueueEventBus;
use InSiteLogic\Event\EventManager;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Api\BootableProviderInterface;
use Silex\Application;

class EventServiceProvider implements ServiceProviderInterface, BootableProviderInterface {

	/**
	 * Bootstraps the application.
	 *
	 * This method is called after all services are registered
	 * and should be used for "dynamic" configuration (whenever
	 * a service must be requested).
	 *
	 * @param Application $app
	 */
	public function boot(Application $app) {
		$app['event-manager.bus'] = function (Application $app) {
			return new GoogleTaskQueueEventBus(
				$app['event-manager.bus.endpoint'],
				$app['identity']->getId()
			);
		};

		$app['event-manager'] = function (Application $app) {
			return new EventManager($app['event-manager.bus']);
		};
	}

	/**
	 * Registers services on the given container.
	 *
	 * This method should only be used to configure services and parameters.
	 * It should not get services.
	 *
	 * @param Container $pimple A container instance
	 */
	public function register(Container $pimple) {
	}
}