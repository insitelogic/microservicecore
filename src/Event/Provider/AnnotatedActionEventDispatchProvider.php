<?php

namespace InSiteLogic\Event\Provider;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use InSiteLogic\Event\Annotations\InSiteEvent;
use InSiteLogic\Event\EventManager;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use ReflectionObject;
use Silex\Api\EventListenerProviderInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class AnnotatedActionEventDispatchProvider implements ServiceProviderInterface, EventListenerProviderInterface {

	public static $controller;

	public static $annotationPath;

	/**
	 * Registers services on the given container.
	 * This method should only be used to configure services and parameters.
	 * It should not get services.
	 * @param Container $pimple A container instance
	 */
	public function register(Container $pimple) {
		$pimple['annotation-reader'] = function (Container $pimple) {
			AnnotatedActionEventDispatchProvider::$annotationPath = $pimple['annotation-reader.vendor-path'] . '/insitelogic/microservicecore/src/Event/Annotations/InSiteEvent.php';
			AnnotationRegistry::registerFile(AnnotatedActionEventDispatchProvider::$annotationPath);
			return new AnnotationReader();
		};
	}

	public function subscribe(Container $pimple, EventDispatcherInterface $dispatcher) {
		$dispatcher->addListener(KernelEvents::CONTROLLER, function (FilterControllerEvent $event) {
			syslog(LOG_INFO, "in CONTROLLER");
			AnnotatedActionEventDispatchProvider::$controller = $event->getController();
		});

		$dispatcher->addListener(KernelEvents::RESPONSE, function (FilterResponseEvent $event) use ($pimple) {
			if(is_array(AnnotatedActionEventDispatchProvider::$controller) && count(AnnotatedActionEventDispatchProvider::$controller) === 2) {
				/** @var AnnotationReader $annotationReader */
				$annotationReader = $pimple['annotation-reader'];

				$instance = AnnotatedActionEventDispatchProvider::$controller[0];
				$methodName = AnnotatedActionEventDispatchProvider::$controller[1];

				$refObj = new ReflectionObject($instance);
				$method = $refObj->getMethod($methodName);

				$annotations = $annotationReader->getMethodAnnotations($method);
				/** @var EventManager $eventManager */
				$eventManager = $pimple['event-manager'];
				require_once AnnotatedActionEventDispatchProvider::$annotationPath;

				foreach ($annotations as $annotation) {
					if($annotation instanceof InSiteEvent) {
						$response = json_decode($event->getResponse()->getContent(), true);
						$data = $response['data'];
						$eventManager->publish($annotation->eventName, $data);
					}
				}
			}

			syslog(LOG_INFO, "in RESPONSE");
		});
	}
}