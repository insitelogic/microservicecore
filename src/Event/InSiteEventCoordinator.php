<?php

namespace InSiteLogic\Event;

use InSiteLogic\Event\EventBus\Impl\GoogleTaskQueueEventBus;

class InSiteEventCoordinator {

	/**
	 * @var EventManager
	 */
	private $eventManager;

	/**
	 * InSiteEventCoordinator constructor.
	 * @param $eventManager
	 */
	public function __construct(EventManager $eventManager) {
		$this->eventManager = $eventManager;
	}

	/**
	 * @return EventManager
	 */
	public function getEventManager() {
		return $this->eventManager;
	}
}