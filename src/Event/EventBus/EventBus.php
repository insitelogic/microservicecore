<?php

namespace InSiteLogic\Event\EventBus;

interface EventBus {

	/**
	 * @param string $eventName
	 * @param array $eventData
	 * @return void
	 */
	public function enqueue($eventName, array $eventData);
}