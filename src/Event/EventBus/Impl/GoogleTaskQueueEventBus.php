<?php

namespace InSiteLogic\Event\EventBus\Impl;

use Exception;
use GuzzleHttp\Client;
use InSiteLogic\Event\EventBus\EventBus;

class GoogleTaskQueueEventBus implements EventBus {

	/**
	 * @var string
	 */
	private $handler;

	/**
	 * @var string
	 */
	private $identity;

	/**
	 * GoogleTaskQueueEventBus constructor.
	 * @param string $handler
	 * @param string $identity
	 */
	public function __construct($handler, $identity) {
		$this->handler = $handler;
		$this->identity = $identity;
	}

	/**
	 * @param string $eventName
	 * @param array  $eventData
	 * @return bool
	 */
	public function enqueue($eventName, array $eventData) {
		$data = $this->pack($eventName, $eventData);

		try {
			$this->pushToQueue($data);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	/**
	 * @param string $eventName
	 * @param array $eventData
	 * @return array
	 */
	private function pack($eventName, array $eventData) {
		$payload = [
			'event' => $eventName,
		    'data' => $eventData
		];

		return $payload;
	}

	/**
	 * @param array $data
	 */
	private function pushToQueue(array $data) {
		syslog(LOG_INFO, "Calling pushToQueue with data as");
		syslog(LOG_INFO, print_r($data, true));
		$client = new Client();
		$response = $client->post($this->handler, [
			'json' => $data,
			'headers' => [
				'IS-Identity' => $this->identity
			]
		]);

		syslog(LOG_INFO, "got response");
		syslog(LOG_INFO, "Status code is: " . $response->getStatusCode());
		syslog(LOG_INFO, "Response body is: " . $response->getBody()->getContents());
	}
}