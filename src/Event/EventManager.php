<?php

namespace InSiteLogic\Event;

use InSiteLogic\Event\EventBus\EventBus;

class EventManager {

	/**
	 * @var EventBus
	 */
	private $eventBus;

	/**
	 * EventManager constructor.
	 * @param EventBus $eventBus
	 */
	public function __construct(EventBus $eventBus) {
		$this->eventBus = $eventBus;
	}

	/**
	 * @param string $eventName
	 * @param array $eventData
	 */
	public function publish($eventName, array $eventData) {
		$this->eventBus->enqueue($eventName, $eventData);
	}
}