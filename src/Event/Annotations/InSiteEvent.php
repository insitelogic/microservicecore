<?php

namespace InSiteLogic\Event\Annotations;

/**
 * @Annotation
 * @Target("METHOD")
 */
class InSiteEvent {

	/**
	 * @var string
	 * @Required
	 */
	public $eventName;

	/**
	 * InSiteEvent constructor.
	 * @param $values
	 */
	public function __construct(array $values) { $this->eventName = $values['eventName']; }
}