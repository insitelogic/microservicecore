<?php

namespace InSiteLogic\Providers;

interface ProviderInterface {
	/**
	 * @param string $token
	 * @param string $isIdentity
	 * @param null|array $data
	 * @return mixed
	 */
	public function request($token, $isIdentity, $data = null);
}