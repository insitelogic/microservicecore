<?php

namespace InSiteLogic\Providers;

use GuzzleHttp\Client;

abstract class AbstractProvider implements ProviderInterface {

	/**
	 * @var Client
	 */
	protected $client;

	/**
	 * @var string
	 */
	protected $baseUrl;

	/**
	 * AbstractProvider constructor.
	 * @param string $baseUrl
	 */
	public function __construct($baseUrl) {
		$this->baseUrl = $baseUrl;

		syslog(LOG_INFO, "Constructing Provider with base_uri of $baseUrl");

		$this->client = new Client(
			[
				'base_uri' => $this->baseUrl
			]
		);
	}


	/**
	 * @param string $token
	 * @param string $isIdentity
	 * @param null|array $data
	 * @return mixed
	 */
	abstract public function request($token, $isIdentity, $data = null);

	/**
	 * @param null|string $param
	 * @return string
	 */
	abstract protected function createRequestPath($param=null);
}