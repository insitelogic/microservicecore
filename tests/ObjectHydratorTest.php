<?php

use InSiteLogic\Util\ObjectHydrator;

class ObjectHydratorTest extends PHPUnit_Framework_TestCase {
	public function testDefaultHydrator() {
		$testClass = ObjectHydrator::getDefaultHydrator()->hydrate(
			['name' => 'Damon', 'age' => 24],
			TestClass::class
		);

		self::assertEquals("Damon", $testClass->getName());
		self::assertEquals(24, $testClass->getAge());
	}
}

class TestClass {

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var int
	 */
	private $age;

	/**
	 * TestClass constructor.
	 *
	 * @param string $name
	 * @param int    $age
	 */
	public function __construct($name, $age) {
		$this->name = $name;
		$this->age  = $age;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @return int
	 */
	public function getAge() {
		return $this->age;
	}
}
