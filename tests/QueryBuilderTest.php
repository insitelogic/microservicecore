<?php

use InSiteLogic\Database\QueryBuilder\QueryBuilder;
use InSiteLogic\Util\ISDBAdapter;

class QueryBuilderTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var mysqli
	 */
	private $mysqli;

	/**
	 * @var ISDBAdapter
	 */
	private $adapter;

	protected function setUp() {
		parent::setUp();
		$this->mysqli = new mysqli("104.155.180.45", "root", "h@MzqWQ3s:", "proj_test");
		$this->adapter = new ISDBAdapter($this->mysqli);
	}

	protected function tearDown() {
		parent::tearDown();
		$this->adapter = null;
		$this->mysqli->close();
	}

	public function testBuildSimpleSelectQuery() {
		$query = QueryBuilder::begin($this->adapter)
			->select(['id', 'email', 'username'])
			->from(['is_auth'])
			->where()
			->equals('id', 1)
			->build();

		$result = $query->fetchSingle();
		self::assertEquals(1, $result['id']);
	}

	public function testBuildSelectWithJoin() {
		$query = QueryBuilder::begin($this->adapter)
			->select(['*'])
			->from(['is_auth'])
			->leftJoin('is_auth_roles')
			->on('is_auth_roles.staff_id', 'is_auth.id')
			->where()
			->equals('is_auth.id', 1)
			->build();

		$result = $query->fetchSingle();
		self::assertEquals(1, $result['id']);
	}

	public function testFunctionCall() {
		$queryResult = QueryBuilder::begin($this->adapter)
			->selectCallback(function (QueryBuilder $queryBuilder) {
				return [
					$queryBuilder->functionCallFragment('count', ['id']),
				    $queryBuilder->functionCallFragment('sum', ['id'])
				];
			})
			->from(['is_auth'])
			->build()
			->fetchSingle();

		$total = array_pop($queryResult);
		$count = array_pop($queryResult);
		self::assertEquals(8, $count);
		self::assertEquals(50, $total);
	}
}