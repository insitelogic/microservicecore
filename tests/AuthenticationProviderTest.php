<?php

use InSiteLogic\Authentication\AuthenticationProvider;

class AuthenticationProviderTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var string
	 */
	private $authenticationProviderBaseUri = "http://authentication-service.insite-projects.appspot.com";

	/**
	 * @var AuthenticationProvider
	 */
	private $authenticationProvider;

	protected function setUp() {
		$this->authenticationProvider = AuthenticationProvider::createProvider($this->authenticationProviderBaseUri);
	}

	public function testAuthenticationProvider() {
		$user = $this->authenticationProvider->request("57c88b68832e9045dbaad50a7b4d143541b39679f74eea9cbb3024301b51fffe8b9f89e6c19794b2141ca89391da0a1ce73ef717842283b63ba4ff68e170e4e6", "1");
		self::assertNotNull($user);
	}
}
